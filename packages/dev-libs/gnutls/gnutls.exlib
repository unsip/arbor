# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_test

SUMMARY="A communications library implementing the SSL, TLS and DTLS protocols"
HOMEPAGE="https://www.${PN}.org/"
DOWNLOADS="https://www.gnupg.org/ftp/gcrypt/${PN}/v$(ever range 1-2)/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
    dane [[ description = [ DNSSEC DANE support for validating certificates ] ]]
    idn
    pkcs11 [[ description = [ Use p11-kit to support multiple external pkcs11 providers ] ]]
"

# many tests are flaky and sorting out the failing ones is a hell to maintain
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/gmp:=
        sys-devel/automake:*[>=1.11.4]
        sys-devel/gettext[>=0.19]
        virtual/pkg-config
        doc? ( dev-doc/gtk-doc[>=1.1] )
    build+run:
        dev-libs/libtasn1[>=4.3]
        dev-libs/libunistring
        dev-libs/nettle:=[>=3.6.0]
        dane? ( net-dns/unbound )
        idn? ( net-dns/libidn2:=[>=2.0.0] )
        pkcs11? ( dev-libs/p11-kit:1[>=0.23.1] )
    test:
        dev-libs/libev[>=4]
        dev-util/cmocka
        dev-util/datefudge
"

WORK=${WORKBASE}/${PN}-$(ever range 1-4)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-local-libopts # else the build fails with autogen installed
    --enable-manpages
    --disable-guile
    --disable-afalg # disables AF_ALG, as we don't yet have libkcapi available
    --disable-openssl-compatibility
    --disable-seccomp-tests
    --disable-static
    --disable-valgrind-tests
    --without-included-unistring
    --without-tpm
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'dane libdane'
    'doc gtk-doc'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    idn
    'pkcs11 p11-kit'
)
DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-tests --disable-tests'
)

# Parallel make fails in doc/
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

AT_M4DIR=( m4 )

gnutls_src_test() {
    esandbox allow_net "LOOPBACK@80"
    esandbox allow_net "LOOPBACK@5559"
    esandbox allow_net --connect "LOOPBACK@5557"
    esandbox allow_net --connect "LOOPBACK@5559"
    esandbox allow_net --connect "inet:127.0.0.1@80"

    emake check

    esandbox disallow_net "LOOPBACK@80"
    esandbox disallow_net "LOOPBACK@5559"
    esandbox disallow_net --connect "LOOPBACK@5557"
    esandbox disallow_net --connect "LOOPBACK@5559"
    esandbox disallow_net --connect "inet:127.0.0.1@80"
}

