# Copyright 2011 Dimitry Ishenko <dimitry.ishenko@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=NixOS release=${PV} suffix=tar.bz2 ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Small utility to modify the dynamic linker and RPATH of ELF executables"
HOMEPAGE="https://nixos.org/patchelf.html"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-fix-roundUp-to-not-truncate-64-bit-values.patch
    "${FILES}"/${PNV}-Use-sh_offset-instead-of-sh_addr-when-checking-alrea.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=( --without-asan )

# Tests might fail because some generated files
# might be missing
DEFAULT_SRC_TEST_PARAMS=( -j1 )

WORK="${WORKBASE}/${PNV}.20200827.8d3a16e"

src_prepare() {
    # remove failing tests, last checked: 0.12
    edo sed \
        -e '/force-rpath.sh/d' \
        -e '/build-id.sh/d' \
        -e 's:output-flag.sh \\:output-flag.sh:' \
        -i tests/Makefile.am

    autotools_src_prepare
}

