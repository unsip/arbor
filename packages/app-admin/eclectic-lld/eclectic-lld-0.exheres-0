# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Copyright 2013 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

SUMMARY="Manages lld's symlinks"
HOMEPAGE="https://www.exherbo.org"
DOWNLOADS=""

LICENCES="GPL-2"
PLATFORMS="~amd64 ~armv7 ~x86"

SLOT="0"

CROSS_COMPILE_TARGETS="
    aarch64-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabihf
    i686-pc-linux-gnu
    i686-pc-linux-musl
    powerpc64-unknown-linux-gnu
    x86_64-pc-linux-gnu
    x86_64-pc-linux-musl
"

MYOPTIONS="
    ( targets: ${CROSS_COMPILE_TARGETS} ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    run:
        app-admin/eclectic[>=2.0.20]
        sys-devel/lld:*
"

WORK="${WORKBASE}"

eclectic-lld_alternatives() {
    local target=${1}

    local alternatives=(
        /usr/$(exhost --target)/bin/${target}-ld   ${target}-ld.lld
        /usr/$(exhost --target)/bin/ld             ld.lld
        "${BANNEDDIR}"/ld                          ld.lld
    )

    alternatives_for ld lld 10 "${alternatives[@]}"
}

src_install() {
    for target in ${CROSS_COMPILE_TARGETS};do
        if option targets:${target};then
            eclectic-lld_alternatives ${target}
        fi
    done
}
