Title: CUPS update
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2020-02-11
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: net-print/cups[<2.3.3_p2]

OpenPrinting CUPS changed the naming of the systemd unit files. If you use
CUPS make sure to enable the required units.

org.cups.cupsd.socket -> cups.socket
org.cups.cupsd.service -> cups.service
org.cups.cupsd.path -> cups.path
org.cups.cups-lpd@.service -> cups-lpd@.service
org.cups.cups-lpd.socket -> cups-lpd.socket

Example:

# systemctl disable org.cups.cupsd.service
# systemctl enable cups.service
